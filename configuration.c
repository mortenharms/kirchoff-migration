#include"configuration.h"
#include<stdio.h>
#include<string.h>
#include <stdlib.h>
#define MAXBUF 1024

void readConfig(char * filename, config *config)
{
	FILE *pFILE;
	char *lineBuf, *delimeterBuf;
	lineBuf = (char*) malloc(MAXBUF);
	
	pFILE = fopen(filename,"r");

	if (pFILE!=NULL)
	{
		printf("open file -%s-\n",filename);
		while(fgets(lineBuf, MAXBUF, pFILE) != NULL)
		{
			if(lineBuf[0] != '#')
			{
				
				if(strstr(lineBuf,"nt"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					config->nt = atoi(delimeterBuf+1);
				}
				else if(strstr(lineBuf,"dz"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					config->dz= atoi(delimeterBuf+1);
				}
				else if(strstr(lineBuf,"ncmp"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					config->ncmp = atoi(delimeterBuf+1);
				}
				else if(strstr(lineBuf,"dt"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					config->dt = atof(delimeterBuf+1);
				}
				else if(strstr(lineBuf,"dcmp"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					config->dcmp = atof(delimeterBuf+1);
				}
				else if(strstr(lineBuf,"vmig"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					config->vmig = atof(delimeterBuf+1);
				}
				else if(strstr(lineBuf,"inputName"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					if(delimeterBuf[1] == ' ')
						sprintf(config->inputName,"%s",delimeterBuf+2);
					else
						sprintf(config->inputName,"%s",delimeterBuf+1);
				}
				if(strstr(lineBuf,"outputName"))
				{
					delimeterBuf = strstr(lineBuf,"=");
					if(delimeterBuf[1] == ' ')
						sprintf(config->outputName,"%s",delimeterBuf+2);
					else
						sprintf(config->outputName,"%s",delimeterBuf+1);
				}
				
			} //end if
		} //end while
		
	} //end if
	
	fclose(pFILE);
	printf("close file -%s-\n",filename);
	return;
}

