CC = gcc
OBJ = main.o alloc.o configuration.o
CFLAGS = -Wall -c 
LFLAGS = -o
prog.x: $(OBJ)
	$(CC) $(LFLAGS) prog.x $(OBJ) -lm

main.o: main.c alloc.o configuration.o
	$(CC) $(CFLAGS) main.c -lm

alloc.o: alloc.c alloc.h
	$(CC) $(CFLAGS) alloc.c

configuration.o: configuration.c configuration.h
	$(CC) $(CFLAGS) configuration.c


