#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "alloc.h"
#include "configuration.h"
#define MAX_STRING_LENGTH 256



int main(int argc, char *argv[])
{
	int nt = 251; /* Anzahl der Zeitsamples */
	int ncmp = 81; /* Anzahl der CMPs */
	float dt = 0.004; /* Samplingintervall [ms] */
	float dcmp = 50; /* CMP-Abstand [m] */
	float vmig = 5000; /* migrationsgeschw. [m/s] */
	float x0, z0, xm;
	float dx, dz; 
	float t;
	int nx, nz, tind;
	float *tmp;
	float **migfield, **seisfield;
	char *inputName="DATA-1";
	char *outputName="migoutput";
	int i, k, l, stat;

	FILE *inputFile, *outputFile;
	config config;
	readConfig("parameters.conf",&config);
	/* INITIALISATIONS AND ALLOCATION*/
	dx = dcmp;	
	dz = 10;

	nx = (ncmp - 1) * dcmp / dx + 1;
	nz = vmig * (nt - 1) * dt / dz;

  
  	/* allocate 1D, 2D and 3D arrays */
	tmp = (float *)alloc1(nt,sizeof(float));
	seisfield = (float **)alloc2(ncmp,nt,sizeof(float));
	migfield = (float **)alloc2(nx,nz,sizeof(float));
	
	/* LOAD DATA */
	inputFile = fopen(inputName,"r");
	
	/* test for errors while opening file */
	if (!inputFile)
	{
		fprintf(stderr, "Unable to open file %s", inputName);
		return 0;
	}


	for (i=0; i<ncmp; i++) 
	{
		stat = fread(tmp,sizeof(float),nt,inputFile);
		if ( stat != nt ) return 0;
	  	for (k=0; k<nt; k++) 
		{
			seisfield[k][i] = tmp[k];
		}
	}
	fclose(inputFile);

	
/* MIGRATION */
	for( k = 0; k < nz; k++)
	{
		z0 = k * dz; /* Tiefe */
		for( i = 0; i < nx; i++)
		{
			x0 = i * dx; /* Offset */
			migfield[k][i] = 0.;
			for (l=0; l<ncmp; l++) 
			{
				xm = l * dcmp;
				/* Berechnung der Laufzeitkurve fuer den  Punkt (x0,z0) */
				t = 2 * sqrt( ( pow( (xm-x0),2 ) + z0 * z0)) / vmig;
				tind = (int)roundf(t/dt);
				printf("t=%f tind=%d \n",t,tind);
				if (tind < nt) migfield[k][i] += seisfield[tind][l];
			}
		}
	}	






/* test ob das einlesen der daten funktioniert */
	outputFile = fopen(outputName,"w");
	if (!inputFile)
	{
		fprintf(stderr, "Unable to open file %s", outputName);
		return 0;
	}
	
	for (i=0; i<nx; i++) 
	{
	  	for (k=0; k<nz; k++) tmp[k]= migfield[k][i];
			fwrite(tmp,sizeof(float),nz,outputFile);
	}
	fclose(outputFile);	

	return 0;
}

























