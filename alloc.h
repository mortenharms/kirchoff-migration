#ifndef __ALLOCATIONS__
#define __ALLOCATIONS__
void *alloc1 (size_t n1, size_t size);            
void **alloc2(size_t n1, size_t n2, size_t size); 
void ***alloc3(size_t n1, size_t n2, size_t n3, size_t size);

#endif
