/* FUNCTIONS FOR ALLOCATION */
#include <stdio.h>
#include <stdlib.h>
#include "alloc.h"
void *alloc1 (size_t n1, size_t size)
{
        void *p;

        if ((p=malloc(n1*size))==NULL)
                return NULL;
        return p;
}

void **alloc2 (size_t n1, size_t n2, size_t size)
{
        size_t i2;
        void **p;

        if ((p=(void**)malloc(n2*sizeof(void*)))==NULL) 
                return NULL;
        if ((p[0]=(void*)malloc(n2*n1*size))==NULL) {
                free(p);
                return NULL;
        }
        for (i2=0; i2<n2; i2++)
                p[i2] = (char*)p[0]+size*n1*i2;
        return p;
}



void ***alloc3 (size_t n1, size_t n2, size_t n3, size_t size)
{
        size_t i3,i2;
        void ***p;

        if ((p=(void***)malloc(n3*sizeof(void**)))==NULL)
                return NULL;
        if ((p[0]=(void**)malloc(n3*n2*sizeof(void*)))==NULL) {
                free(p);
                return NULL;
        }
        if ((p[0][0]=(void*)malloc(n3*n2*n1*size))==NULL) {
                free(p[0]);
                free(p);
                return NULL;
        }

        for (i3=0; i3<n3; i3++) {
                p[i3] = p[0]+n2*i3;
                for (i2=0; i2<n2; i2++)
                        p[i3][i2] = (char*)p[0][0]+size*n1*(i2+n2*i3);
        }
        return p;
}
