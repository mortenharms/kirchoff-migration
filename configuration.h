#ifndef __configuration__
#define __configuration__

/* STRUCTURES */
typedef struct
{
	int nt; /* Anzahl der Zeitsamples */
	int dz; /* diskretisierung in der Tiefe */
	int ncmp; /* Anzahl der CMPs */
	float dt; /* Samplingintervall [ms] */
	float dcmp; /* CMP-Abstand [m] */
	float vmig; /* migrationsgeschw. [m/s] */
	char *inputName;
	char *outputName;

}config;

/* FUNCTIONS */
void readConfig(char * filename, config *configurations);

#endif
